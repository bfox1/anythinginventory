package io.bfox.anythinginventory.action;

import io.bfox.anythinginventory.AnythingInventory;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by bfox1 on 9/17/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 *
 * The Kit Class contains a List of ItemStacks to provide as a Kit.
 */
public class Kit {

    private List<ItemStack> itemStacks = new ArrayList<>();

    private final AnythingInventory plugin;

    public Kit(AnythingInventory plugin)
    {
        this.plugin = plugin;
    }

    /**
     * Returns the AnythingInventory Instance.
     * @return AnythingInventory.
     */
    public AnythingInventory getPlugin() {
        return plugin;
    }

    /**
     * Sets the Kit to the Player.
     * @param player The Player in which the Kit will be set to.
     */
    public void setKitToPlayer(Player player)
    {
        player.getInventory().clear();
        if(player.getOpenInventory() != null)
        {
            player.closeInventory();
        }
        for(ItemStack stack : itemStacks)
        {
            player.getInventory().addItem(stack);
        }
    }

    /**
     * Adds ItemStack(s) to the Kit Class.
     * @param stack ItemStacks.
     */
    public void addToKit(ItemStack... stack)
    {
        for(ItemStack stacks : stack)
        {
            if(stacks != null)
            {
                itemStacks.add(stacks);
            }
        }
    }
}

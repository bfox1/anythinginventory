package io.bfox.anythinginventory.action;

import io.bfox.anythinginventory.AnythingInventory;
import io.bfox.anythinginventory.exceptions.InvalidItemActionException;
import io.bfox.anythinginventory.inventory.InventoryMenu;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.io.Serializable;

/**
 * Created by bfox1 on 8/1/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public enum ItemAction implements Serializable{


    COMMAND(0, "Action.Command")
            {
                @Override
                public void fireItemAction(Player player, Object object)
                {
                    if(player != null && object instanceof String)
                    {
                        Bukkit.dispatchCommand(player, (String) object);
                    }
                    else
                    {
                        throw new InvalidItemActionException("Action must not be a command!" +
                                " Please check the Item Settings again", getId());
                    }
                }
            },
    ITEM(1, "Action.Item")
            {
                @Override
                public void fireItemAction(Player player, Object object)
                {
                    if(player != null && object instanceof ItemStack)
                    {
                        player.getInventory().addItem((ItemStack)object);
                        player.closeInventory();
                        player.sendMessage("The Item has been added to your inventory!");
                    }
                    else
                    {
                        throw new InvalidItemActionException("Action must not be an ItemStack!" +
                                "Please check the Item Settings to fix this problem!", getId());
                    }
                }
            },
    TELEPORT(2, "Action.Teleport")
            {
                @Override
                public void fireItemAction(Player player, Object object)
                {
                    if(player != null && object instanceof Location)
                    {
                        player.teleport((Location)object);
                    }
                    else
                    {
                        throw new InvalidItemActionException("Action must not be a Location!" +
                                "Please check the Item Settings to fix this problem!", getId());
                    }
                }
            },
    INVENTORY(3, "Action.Inventory")
            {
                @Override
                public void fireItemAction(Player player, Object object)
                {
                    if(player != null && object instanceof InventoryMenu)
                    {
                        InventoryMenu menu = (InventoryMenu)object;
                        if(AnythingInventory.getInstance().hasMenu(menu.getPlugin(), menu.getMenuName()))
                        {
                            player.closeInventory();
                            Inventory invo =  AnythingInventory.getInstance().getMenu(menu.getPlugin(), menu.getMenuName()).getInventory();
                            player.openInventory(invo);
                        }
                    }
                }
            },
    KIT(4, "Action.Kit")
            {
                @Override
                public void fireItemAction(Player player, Object object)
                {
                    if(player != null && object instanceof Kit)
                    {
                        if(object != null)
                        ((Kit)object).setKitToPlayer(player);
                    }
                }
            },
    NONE(5, "Action.None")
            {
               @Override
                public void fireItemAction(Player player, Object object)
               {
                   if(player != null)
                   {
                       //Do nothing :P
                   }
               }
            };



    public abstract void fireItemAction(Player player, Object object);
    private int id;
    private String actionName;

    ItemAction(int id, String actionName)
    {
        this.id = id;
        this.actionName = actionName;
    }

    public int getId()
    {
        return id;
    }

    public String getActionName()
    {
        return actionName;
    }

    public static ItemAction getItemAction(int id)
    {
        for(ItemAction action : ItemAction.values())
        {
            if(action.getId() == id)
            {
                return action;
            }
        }
        throw new NullPointerException("Invalid id Type for Item Action! " + id + " is not an action");
    }

    public static ItemAction getItemAction(String name)
    {
        for(ItemAction action : ItemAction.values())
        {
            if(action.getActionName().equalsIgnoreCase(name))
            {
                return action;
            }
        }
        return null;
    }


    @Override
    public String toString()
    {
        return String.valueOf(id);
    }


}

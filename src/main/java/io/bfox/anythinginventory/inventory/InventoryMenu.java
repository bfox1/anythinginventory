package io.bfox.anythinginventory.inventory;

import io.bfox.anythinginventory.AnythingInventory;
import io.bfox.anythinginventory.ItemIcon;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLongArray;

import static io.bfox.anythinginventory.ObjectValidation.*;

/**
 * Created by bfox1 on 8/1/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class InventoryMenu implements Cloneable{


    private LinkedHashMap<Integer, ItemIcon> itemIconInventoryList;
    private String menuName;
    private int inventorySize;
    private Inventory inventory;
    private final JavaPlugin plugin;
    private boolean acceptedToAll = true;
    private boolean canMoveFrom = false;
    private boolean canMoveTo = false;

    public InventoryMenu(String menuName, int inventorySize, JavaPlugin plugin)
    {
        this.plugin = plugin;
        this.itemIconInventoryList = new LinkedHashMap<>();
        this.inventory = buildInventory(inventorySize, menuName);
        this.inventorySize = inventorySize;
        this.menuName = inventory.getName();
    }

    public InventoryMenu(Inventory inventory, AnythingInventory plugin)
    {
        this.inventory = inventory;
        this.menuName = inventory.getName();
        inventorySize = inventory.getSize();
        this.plugin = plugin;
    }




    /**
     * Creates the Inventory For the InventoryMenu
     * @param invetorySize Slots the Inventory will have.
     * @param inventoryName The Inventory Name.
     * @return Inventory
     */
    private Inventory buildInventory(int invetorySize, String inventoryName)
    {
        return Bukkit.getServer().createInventory(null, invetorySize, inventoryName);
    }

    /**
     * Returns a List of ItemIcon.
     * @return List
     */
    public Map<Integer,ItemIcon> getItemIconInventoryList() {
        return itemIconInventoryList;
    }

    public ItemIcon getItemIcon(ItemStack stack)
    {
        if(containsItem(stack))
        {
            for(Map.Entry icon : itemIconInventoryList.entrySet())
            {
                ItemIcon ic = (ItemIcon)icon.getValue();
                if(ic.getItemStack().equals(stack))
                {
                    return ic;
                }
            }
        }
        return null;
    }

    /**
     * Sets the List. This is irrelevant usage and may be removed if no usage required.
     * @param itemIconInventoryList
     */
    public void setItemIconInventoryList(LinkedHashMap<Integer, ItemIcon> itemIconInventoryList) {
        this.itemIconInventoryList = itemIconInventoryList;
    }

    /**
     * Returns the Inventory of the InventoryMenu
     * @return Inventory.
     */
    public Inventory getInventory()
    {
        this.restoreInventory();
        return inventory;
    }

    /**
     * Returns the InventoryName
     * @return String
     */
    public String getMenuName() {
        return menuName;
    }

    public boolean containsItem(ItemStack stack)
    {
        return inventory.contains(stack);
    }

    public boolean containsItem(String itemName)
    {
        for(ItemStack stack : inventory.getContents()) {
            return stack.getItemMeta().hasDisplayName() && stack.getItemMeta().getDisplayName().equalsIgnoreCase(itemName);
        }
        return false;
    }

    public void addItemIcon(ItemIcon icon)
    {
        checkNotNull(icon);

        while(itemIconInventoryList.containsKey(icon.getIconID()))
        {
            icon.setIconID(icon.getIconID() + 1);
        }
        itemIconInventoryList.put(icon.getIconID(), icon);
    }

    public void addItem(ItemStack... stack)
    {
        for(ItemStack stacks : stack)
        {
            assert stacks != null;
            inventory.addItem(stacks);
        }
    }

    /**
     * Updates the entire Class with a new name and or Inventory Size.
     */
    public void update()
    {
        if(!menuName.equalsIgnoreCase(inventory.getName()))
        {
            ItemStack[] stack = inventory.getContents();
            this.inventory = Bukkit.createInventory(null,9, menuName);
            inventory.addItem(stack);
        }
    }

    /**
     * Will reset the Inventory to its default state, this is to prevent an option if a player adds any items to
     * inventory, it will remove them.
     */
    public void restoreInventory()
    {
        int it = 0;
        this.inventory.clear();
        for(Map.Entry icon : itemIconInventoryList.entrySet())
        {

            if(inventory == null)
            {
                this.inventory = Bukkit.createInventory(null, inventorySize, menuName);
            }
            if(icon != null)
            {
                ItemIcon itemIcon = (ItemIcon)icon.getValue();
                this.inventory.setItem(it, itemIcon.getItemStack());
            }
            it++;
        }
    }

    /**
     * Sets the Invetory Name.
     * @param menuName The Name of the Inventory
     */
    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public boolean isAcceptedToAll() {
        return acceptedToAll;
    }

    public void setAcceptedToAll(boolean acceptedToAll) {
        this.acceptedToAll = acceptedToAll;
    }

    public boolean isCanMoveFrom() {
        return canMoveFrom;
    }

    public void setCanMoveFrom(boolean canMoveFrom) {
        this.canMoveFrom = canMoveFrom;
    }

    public boolean isCanMoveTo() {
        return canMoveTo;
    }

    public void setCanMoveTo(boolean canMoveTo) {
        this.canMoveTo = canMoveTo;
    }

    public JavaPlugin getPlugin() {
        return plugin;
    }

    /**
     * To modify the Menu for a specific reason without directly modifying the original Menu.
     * Best used when checking for player or other instance based updates to Menu. (Example: Menu loads a default Menu
     * of Items with itemNames, Player has custom names on the Items, therefore, we use clone() to create an instance of
     * the original and modify it then present it to the Player without modifying the Server Menu.)
     * @return A Cloned version of InventoryMenu
     * @throws CloneNotSupportedException
     */
    @Override
    public InventoryMenu clone()
    {
        try {
            return (InventoryMenu)super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }

    }

}

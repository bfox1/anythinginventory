package io.bfox.anythinginventory;

import io.bfox.anythinginventory.action.ItemAction;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Created by bfox1 on 10/10/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class ItemPerformer
{

    private ItemStack itemStack;
    private String itemName;

    private final ItemAction action;
    private final Object DEF_ACTION_PARAM, RIGHT_ACTION_PARAM, SHIFT_RIGHT_ACTION_PARAM, SHIFT_LEFT_ACTION_PARAM;

    private CustomAction customAction;

    private boolean isLeftClicked, isRightClicked, isShiftedRightClicked, isShiftLeftClicked;


    private final boolean isItemAction;

    private boolean isTimedConstrained;


    /**
     * For default Action with anyClick action.
     * @param stack
     * @param itemName
     * @param itemLore
     * @param action
     * @param DEF_ACTION_PARAM
     */
    public  ItemPerformer(ItemStack stack, String itemName, List<String> itemLore, ItemAction action, Object DEF_ACTION_PARAM)
    {
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(itemName);
        meta.setLore(itemLore);
        stack.setItemMeta(meta);

        this.itemStack = stack;
        this.itemName = itemName;
        this.action = action;
        this.DEF_ACTION_PARAM = DEF_ACTION_PARAM;
        this.RIGHT_ACTION_PARAM = null;
        this.SHIFT_RIGHT_ACTION_PARAM = null;
        this.SHIFT_LEFT_ACTION_PARAM = null;
        this.isItemAction = true;
        this.isLeftClicked = false;
        this.isRightClicked = false;
        this.isShiftedRightClicked = false;
        this.isShiftLeftClicked = false;
    }

    public ItemPerformer(ItemStack stack, String itemName, List<String> itemLore, ItemAction action, Object leftAction, Object rightAction, Object shiftRightAction, Object shiftLeftAction)
    {
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(itemName);
        meta.setLore(itemLore);
        stack.setItemMeta(meta);

        this.itemStack = stack;
        this.itemName = itemName;
        this.action = action;
        this.DEF_ACTION_PARAM = leftAction;
        this.RIGHT_ACTION_PARAM = rightAction;
        this.SHIFT_RIGHT_ACTION_PARAM = shiftRightAction;
        this.SHIFT_LEFT_ACTION_PARAM = shiftLeftAction;
        this.isItemAction = true;
        this.isLeftClicked = leftAction != null;
        this.isRightClicked = rightAction != null;
        this.isShiftedRightClicked = shiftRightAction != null;
        this.isShiftLeftClicked = shiftLeftAction != null;
    }

    public  ItemPerformer(ItemStack stack, String itemName, List<String> itemLore, CustomAction action, Object DEF_ACTION_PARAM)
    {
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(itemName);
        meta.setLore(itemLore);
        stack.setItemMeta(meta);

        this.itemStack = stack;
        this.itemName = itemName;
        this.customAction = action;
        this.action = null;
        this.DEF_ACTION_PARAM = DEF_ACTION_PARAM;
        this.RIGHT_ACTION_PARAM = null;
        this.SHIFT_RIGHT_ACTION_PARAM = null;
        this.SHIFT_LEFT_ACTION_PARAM = null;
        this.isItemAction = false;
        this.isLeftClicked = false;
        this.isRightClicked = false;
        this.isShiftedRightClicked = false;
        this.isShiftLeftClicked = false;
    }

    public ItemPerformer(ItemStack stack, String itemName, List<String> itemLore, CustomAction action, Object leftAction, Object rightAction, Object shiftRightAction, Object shiftLeftAction)
    {
        ItemMeta meta = stack.getItemMeta();
        meta.setDisplayName(itemName);
        meta.setLore(itemLore);
        stack.setItemMeta(meta);

        this.itemStack = stack;
        this.itemName = itemName;
        this.customAction = action;
        this.action = null;
        this.DEF_ACTION_PARAM = leftAction;
        this.RIGHT_ACTION_PARAM = rightAction;
        this.SHIFT_RIGHT_ACTION_PARAM = shiftRightAction;
        this.SHIFT_LEFT_ACTION_PARAM = shiftLeftAction;
        this.isItemAction = false;
        this.isLeftClicked = leftAction != null;
        this.isRightClicked = rightAction != null;
        this.isShiftedRightClicked = shiftRightAction != null;
        this.isShiftLeftClicked = shiftLeftAction != null;
    }

    public ItemPerformer(ItemStack stack, ItemAction action, Object defAction)
    {
        this(stack, stack.getItemMeta().getDisplayName(), stack.getItemMeta().getLore(), action, defAction);
    }

    public ItemPerformer(ItemStack stack, ItemAction action, Object leftAction, Object rightAction, Object shiftRightAction, Object shiftLeftAction)
    {
        this(stack, stack.getItemMeta().getDisplayName(), stack.getItemMeta().getLore(), action, leftAction, rightAction, shiftRightAction, shiftLeftAction);
    }

    public ItemPerformer(ItemStack stack, CustomAction action, Object defAction)
    {
        this(stack, stack.getItemMeta().getDisplayName(), stack.getItemMeta().getLore(), action, defAction);
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public ItemAction getAction() {
        return action;
    }

    public Object getDEF_ACTION_PARAM() {
        return DEF_ACTION_PARAM;
    }

    public boolean isLeftClicked() {
        return isLeftClicked;
    }


    public boolean isRightClicked() {
        return isRightClicked;
    }


    public boolean isItemAction() {
        return isItemAction;
    }

    public Object getRIGHT_ACTION_PARAM() {
        return RIGHT_ACTION_PARAM;
    }

    public Object getSHIFT_RIGHT_ACTION_PARAM() {
        return SHIFT_RIGHT_ACTION_PARAM;
    }

    public Object getSHIFT_LEFT_ACTION_PARAM() {
        return SHIFT_LEFT_ACTION_PARAM;
    }

    public boolean isShiftedRightClicked() {
        return isShiftedRightClicked;
    }

    public boolean isShiftLeftClicked() {
        return isShiftLeftClicked;
    }


    public void setClickAction(String clickAction)
    {
        switch (clickAction)
        {
            case "left":
                this.isLeftClicked = true;
                this.isRightClicked = false;
                this.isShiftedRightClicked = false;
                this.isShiftLeftClicked = false;
                break;
            case "right":
                this.isLeftClicked = false;
                this.isRightClicked = true;
                this.isShiftedRightClicked = false;
                this.isShiftLeftClicked = false;
                break;
            case "shiftLeft":
                this.isLeftClicked = false;
                this.isRightClicked = false;
                this.isShiftedRightClicked = false;
                this.isShiftLeftClicked = true;
                break;
            case "shiftRight":
                this.isLeftClicked = false;
                this.isRightClicked = false;
                this.isShiftedRightClicked = true;
                this.isShiftLeftClicked = false;
                break;
            default:
                throw new IllegalArgumentException("clickAction Must be left, right, or shift!");
        }
    }

    public void fireItemAction(Player player, Object o)
    {
        if(action != null && o != null)
        {
            action.fireItemAction(player, o);
        }
        else if(customAction != null && o != null)
        {
            customAction.fireCustomAction(player, o);
        }
        else
        {
            throw new NullPointerException("Something happened! Either Player ItemIcons Action or Action Params are null!");
        }
    }

    public boolean isTimedConstrained() {
        return isTimedConstrained;
    }

    public void setIsTimedConstrained(boolean isTimedConstrained) {
        this.isTimedConstrained = isTimedConstrained;
    }
}

package io.bfox.anythinginventory.events;

import io.bfox.anythinginventory.ItemIcon;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by bfox1 on 11/12/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class ItemActionEvent extends Event implements Cancellable
{
    private static final HandlerList handlers = new HandlerList();
    private final ItemIcon icon;
    private final Player player;
    private boolean cancelled;

    public ItemActionEvent(ItemIcon icon, Player player)
    {
        this.icon = icon;
        this.player = player;
    }
    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }

    public ItemIcon getIcon() {
        return icon;
    }


    public Player getPlayer() {
        return player;
    }

}

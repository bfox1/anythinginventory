package io.bfox.anythinginventory.events;

import io.bfox.anythinginventory.ItemIcon;
import io.bfox.anythinginventory.inventory.InventoryMenu;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Created by bfox1 on 11/12/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class ItemIconClickedEvent extends Event implements Cancellable {

    private static final HandlerList handlers = new HandlerList();
    private final ItemIcon icon;
    private final Player player;
    private final InventoryMenu menu;
    private boolean cancelled;


    public ItemIconClickedEvent(Player player, ItemIcon icon, InventoryMenu menu)
    {
        this.player = player;
        this.icon = icon;
        this.menu = menu;
    }




    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean b) {
        this.cancelled = b;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }

    public ItemIcon getClickedIcon() {
        return icon;
    }

    public InventoryMenu getClickedMenu()
    {
        return menu;
    }


    public Player getPlayer() {
        return player;
    }
}

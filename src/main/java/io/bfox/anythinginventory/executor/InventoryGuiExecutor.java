package io.bfox.anythinginventory.executor;

import io.bfox.anythinginventory.AnythingInventory;
import io.bfox.anythinginventory.utility.ChatUtility;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import static io.bfox.anythinginventory.utility.ExecutorUtility.checkCommand;

/**
 * Created by bfox1 on 8/27/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class InventoryGuiExecutor implements CommandExecutor {

    private AnythingInventory plugin;

    private final String starOperator = "*";

    public InventoryGuiExecutor(AnythingInventory plugin) {
        this.plugin = plugin;
    }


    public AnythingInventory getPlugin() {
        return plugin;
    }

    @Override
    public boolean onCommand(CommandSender commandSender, Command command, String s, String[] strings) {

       /* if (commandSender instanceof Player) {
            Player player = (Player) commandSender;
            if (command.getName().equalsIgnoreCase("AnythingInventory"))
            {

                if(strings.length == 0) //Returns a list of commands to be executed.
                {
                    player.sendMessage(ChatUtility.PLUGIN_NAME + "Example commands will be issued below!");
                    return true;
                }


                if(strings.length == 1) //For any Sub-Commands with only 1 String ahead of the main Command.
                {
                    if(checkCommand(strings, 0, "getInventories", "getInvos")) // Returns a list of all Inventories.
                    {
                        player.sendMessage(ChatUtility.PLUGIN_NAME + InventoryData.getInventoryList());
                        return true;
                    }
                }


                if(strings.length == 2) // For any Sub-Commands with only 2 Strings ahead of the Main Command.
                {
                    if(checkCommand(strings, 0, "open")) //Opens inventories that have been listed.
                    if (plugin.getMenu(strings[1]) != null )
                    {
                        InventoryMenu inventory = plugin.getMenu(strings[1]);
                        if(inventory.isAcceptedToAll()
                                && verifyPermission("AnythingInventory.openInventory", player)) // Checks to see if the InventoryMenu is accepted to all
                        {
                            player.openInventory(plugin.getMenu(strings[1]).getMenu());
                            return true;
                        }
                        else if(verifyPermission("AnythingInventory." + inventory.getMenuName(), player)) //Verifies Player Permissions for locked inventory.
                        {
                            player.openInventory(inventory.getMenu());
                            return true;
                        }
                        else
                        {
                            player.sendMessage(ChatUtility.PLUGIN_NAME + ChatColor.RED + ChatColor.BOLD
                                    + "Sorry, but you dont have permission to open that inventory!");
                            return false;
                        }
                    }
                    else if(plugin.getConfigMenu(player.getUniqueId()) != null)
                    {
                        InventoryMenu inventory = plugin.getConfigMenu(player.getUniqueId());
                        if(verifyPermission("AnythingInventory." + inventory.getMenuName(), player) &&
                                getPlugin().getInventoryPlayer(player.getUniqueId()) != null)
                        {
                            if(getPlugin().getInventoryPlayer(player.getUniqueId()).isInEditMode())
                            {
                                player.openInventory(getPlugin().getConfigMenu(player.getUniqueId()).getMenu());
                                return true;
                            }
                        }
                    }
                    else
                    {
                        player.sendMessage(ChatUtility.PLUGIN_NAME + ChatColor.WHITE + strings[1]
                                + ChatColor.DARK_BLUE + " does not exist!");
                        return false;
                    }


                    if(checkCommand(strings,0, "publish"))
                    if(verifyPermission("AnythingInventory.Publish", player))
                    {
                        InventoryMenu menu = getPlugin().getConfigMenu(player);
                        InventoryPlayer iPlayer = getPlugin().getInventoryPlayer(player.getUniqueId());
                        if(iPlayer != null) {
                            iPlayer.setInEditMode(false);
                            getPlugin().getInventoryHashMap().put(menu.getMenuName(), menu);
                            getPlugin().getEditableInventory().remove(iPlayer);
                            player.sendMessage(ChatUtility.PLUGIN_NAME + "Your Inventory has now been published!");
                            return true;
                        }
                        return false;
                    }
                    else
                    {
                        player.sendMessage(commandDeniedMessage("Sorry, but you are not allowed to Publish Inventories!"));
                        return false;
                    }

                    if(checkCommand(strings, 0, "edit"))
                    if(verifyPermission("AnythingInventory.edit.*", player) || verifyPermission("AnythingInventory.edit." + strings[1], player))
                    {
                        InventoryPlayer iPlayer = new InventoryPlayer(getPlugin(), player);
                        if(inventoryExists(strings[1]))
                        {
                            InventoryMenu menu = getPlugin().getMenu(strings[1]);
                            iPlayer.setInEditMode(true);
                            iPlayer.setEditingInvoName(menu.getMenuName());
                            getPlugin().getEditableInventory().put(iPlayer, menu);
                            getPlugin().getInventoryHashMap().remove(menu.getMenuName());
                            return true;
                        }
                        else return false;
                    }

                    if(checkCommand(strings, 0, "setItemIcon"))
                        if(verifyPermission("AnythingInventory.setitemicon", player))
                        {
                            return true;
                        }
                }

                if(strings.length == 3)
                {
                    if(checkCommand(strings, 0, "create"))
                    if (verifyPermission("AnythingInventory.create", player))
                    {
                        if(inventoryExists(strings[1]))
                        {
                            player.sendMessage(ChatUtility.PLUGIN_NAME + "The Inventory Already Exists!");
                        }
                        int row;
                        try
                        {
                            row = Integer.valueOf(strings[2]);
                        }
                        catch(NumberFormatException e)
                        {
                            player.sendMessage(ChatUtility.PLUGIN_NAME + "You must input a Number!" +
                                    " Try /AnythingInventory create" + strings[1] + " " + 9 + " ?");
                            return false;
                        }

                        InventoryMenu menu = new InventoryMenu(strings[1], row);
                        menu.setAcceptedToAll(false);

                        InventoryPlayer iPlayer = new InventoryPlayer(getPlugin(), player);
                        iPlayer.setInEditMode(true);
                        plugin.getEditableInventory().put(iPlayer,menu);
                        player.sendMessage(ChatUtility.PLUGIN_NAME + "You have created " + strings[1] +
                        " To edit your new inventory do /AnythingInventory edit <InventoryName>");
                        return true;
                    }
                    else
                    {
                        player.sendMessage(commandDeniedMessage("Sorry, but you are not allowed to create Inventories!"));
                        return false;
                    }

                    if(checkCommand(strings, 0 , "buildIcon"))
                        if(verifyPermission("AnythignInventory.buildicon", player))
                        {
                            if(strings[2].equalsIgnoreCase(ItemAction.ITEM.getActionName().substring(5)) ||
                                    strings[2].equalsIgnoreCase(ItemAction.COMMAND.getActionName().substring(5)) ||
                                    strings[2].equalsIgnoreCase(ItemAction.TELEPORT.getActionName().substring(5)) ||
                                    strings[2].equalsIgnoreCase(ItemAction.INVENTORY.getActionName().substring(5)))
                            {
                                InventoryPlayer iPlayer = getPlugin().getInventoryPlayer(player.getUniqueId());
                                assert iPlayer != null;
                                iPlayer.setBuildingIcon(new ItemIcon(player.getItemInHand(), ItemAction.getItemAction(strings[2]), null));
                                ItemMeta meta = player.getItemInHand().getItemMeta();
                                meta.setDisplayName(strings[1]);
                                player.getItemInHand().setItemMeta(meta);
                                player.sendMessage(ChatUtility.PLUGIN_NAME + "You have set this Item as an Icon. Use various commands to edit the Icon.");

                                return true;
                            }
                            else return false;

                        }
                        else return false;
                }

            }
            player.sendMessage(ChatUtility.PLUGIN_NAME + "Hmmm, Maybe you didn't add enough arguments?");
        }*/
        return false;
    }


    public boolean verifyPermission(String permission, Player player)
    {
        if(player.hasPermission("AnythingInventory." + starOperator)) return true;
        if(player.hasPermission("AnythingInventory.openInventory." + starOperator)) return true;
        if(!player.hasPermission(permission))
        {
            return false;
        }
        return true;
    }


    public boolean checkCommand(String[] stringArray, int index, String... commandSuffix)
    {
        for (String s : commandSuffix)
        {
            if(stringArray[index].equalsIgnoreCase(s))
            {
                return true;
            }
        }
        return false;
    }

    public boolean checkCommand(String[] stringArray, int subCommandIndex, int commandInputLength, String... subCommands)
    {
        return checkCommand(stringArray, subCommandIndex, subCommands) && stringArray.length == commandInputLength;
    }


    public String commandDeniedMessage(String msg)
    {
        return ChatUtility.PLUGIN_NAME + ChatColor.RED + ChatColor.BOLD + msg;
    }






}

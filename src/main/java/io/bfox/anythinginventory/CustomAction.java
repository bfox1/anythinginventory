package io.bfox.anythinginventory;

import org.bukkit.entity.Player;

/**
 * Created by bfox1 on 10/9/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public interface CustomAction
{

     void fireCustomAction(Player player, Object object);

     String getName();
}

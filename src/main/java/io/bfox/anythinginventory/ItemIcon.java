package io.bfox.anythinginventory;

import io.bfox.anythinginventory.action.ItemAction;
import io.bfox.anythinginventory.events.ItemActionEvent;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.List;

/**
 * Created by bfox1 on 8/2/2015.
 */
public class ItemIcon{


    private ItemStack itemStack;
    private String itemName;

    private final ItemAction action;
    private final Object actionParam;

    private final CustomAction customAction;

    private boolean isLeftClicked;
    private boolean isRightClicked;
    private boolean isShiftClicked;

    private final boolean isItemAction;

    private int iconID = 0;

    /**
     * Represents a Full Blown Item Icon with a Lore attached to it.
     * @param displayStack The ItemStack you wish to display.
     * @param itemName A Custom Item Name.
     * @param lore The Lore of the Item.
     * @param action The Action that will perform when the Item is clicked on.
     * @param actionParam The Fired Action that will occur.
     */
    public ItemIcon(ItemStack displayStack, String itemName, List<String> lore, ItemAction action, Object actionParam)
    {
        ItemMeta meta = displayStack.getItemMeta();
        meta.setDisplayName(itemName);
        meta.setLore(lore);
        displayStack.setItemMeta(meta);
        this.itemStack = displayStack;
        this.itemName = itemName;
        this.action = action;
        this.actionParam = actionParam;
        this.isItemAction = true;
        this.setClickAction("left");


        this.customAction = null;
    }

    public ItemIcon(ItemStack displayStack, String itemName, List<String> lore, CustomAction action, Object actionParam)
    {
        ItemMeta meta = displayStack.getItemMeta();
        meta.setDisplayName(itemName);
        meta.setLore(lore);
        displayStack.setItemMeta(meta);
        this.itemStack = displayStack;
        this.itemName = itemName;
        this.action = null;
        this.customAction = action;
        this.actionParam = actionParam;
        this.isItemAction = false;
        this.setClickAction("left");


    }

    /**
     * Represents an alternate for the ItemIcon.
     * @param material The ItemStack you wish to display.
     * @param itemName A Custom Item Name.
     * @param lore The Lore of the Item.
     * @param action The Action that will perform when the Item is clicked on.
     * @param actionParam The Fired Action that will occur.
     */
    public ItemIcon(Material material, String itemName, List<String> lore, ItemAction action, Object actionParam)
    {
        this(new ItemStack(material), itemName, lore, action, actionParam);

    }

    public ItemIcon(Material material, String itemName, List<String> lore, CustomAction action, Object actionParam)
    {
        this(new ItemStack(material), itemName, lore, action, actionParam);
    }


    public ItemIcon(ItemIcon icon, ItemAction action, Object actionParam)
    {
        this(icon.getItemStack(), action, actionParam);
        this.isLeftClicked = icon.isLeftClicked;
        this.isRightClicked = icon.isRightClicked;
        this.isShiftClicked = icon.isShiftClicked;


    }

    /**
     * Represents a lessened version of the ItemIcon Constructor.
     * @param stack The ItemStack in which will be displayed
     * @param action The ItemAction that will be fired when clicked on.
     * @param actionParam The Parameters the Item will take when clicked on.
     */
    public ItemIcon(ItemStack stack, ItemAction action, Object actionParam)
    {
        this.itemStack = stack;
        this.itemName = itemStack.getItemMeta().getDisplayName();
        this.action = action;
        this.actionParam = actionParam;
        this.isItemAction = true;
        this.setClickAction("left");
        this.customAction = null;

    }

    public ItemIcon(ItemStack stack, CustomAction customAction, Object actionParam)
    {
        this.itemStack = stack;
        this.itemName = itemStack.getItemMeta().getDisplayName();
        this.actionParam = actionParam;
        this.customAction = customAction;
        this.action = null;
        this.isItemAction = false;
        this.setClickAction("left");
    }


    /**
     * Returns the ItemStack.
     * @return The ItemStack.
     */
    public ItemStack getItemStack() {
        return itemStack;
    }

    /**
     * Returns the String of the ItemStack.
     * @return the ItemStack Custom name or default name.
     */
    public String getItemName()
    {
        return itemName;
    }

    /**
     * Sets the ItemStack.
     * @param itemStack The ItemStack.
     */
    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    /**
     * Returns the ItemAction of the Item.
     * @return The Action.
     */
    public ItemAction getAction() {
        return action;
    }

    /**
     * Returns the Action Parameters the Action will take when clicked on
     * @return The Action Parameters will return as Object.
     */
    public Object getActionParam() {
        return actionParam;
    }

    public void fireItemAction(Player player)
    {
        ItemActionEvent event = new ItemActionEvent(this, player);
        AnythingInventory.getInstance().getPluginManager().callEvent(event);
        if(action != null && actionParam != null)
        {
            action.fireItemAction(player, actionParam);
        }
        else if(customAction != null && actionParam != null)
        {
            customAction.fireCustomAction(player, actionParam);
        }
        else
        {
            throw new NullPointerException("Something happened! Either Player ItemIcons Action or Action Params are null!");
        }
    }

    public void setClickAction(String clickAction)
    {
        switch (clickAction)
        {
            case "left":
                this.isLeftClicked = true;
                this.isRightClicked = false;
                this.isShiftClicked = false;
                break;
            case "right":
                this.isLeftClicked = false;
                this.isRightClicked = true;
                this.isShiftClicked = false;
                break;
            case "shift":
                this.isLeftClicked = false;
                this.isRightClicked = false;
                this.isShiftClicked = true;
                break;
            default:
                throw new IllegalArgumentException("clickAction Must be left, right, or shift!");
        }
    }


    public boolean isLeftClicked() {
        return isLeftClicked;
    }

    public boolean isRightClicked() {
        return isRightClicked;
    }

    public boolean isShiftClicked() {
        return isShiftClicked;
    }

    public boolean isItemAction() {
        return isItemAction;
    }

    public CustomAction getCustomAction() {
        return customAction;
    }

    public int getIconID() {
        return iconID;
    }

    public void setIconID(int iconID) {
        this.iconID = iconID;
    }
}

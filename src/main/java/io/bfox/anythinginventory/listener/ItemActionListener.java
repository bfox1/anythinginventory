package io.bfox.anythinginventory.listener;

import io.bfox.anythinginventory.AnythingInventory;
import io.bfox.anythinginventory.ItemIcon;
import io.bfox.anythinginventory.events.ItemIconClickedEvent;
import io.bfox.anythinginventory.inventory.InventoryMenu;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.omg.CORBA.Any;

/**
 * Created by bfox1 on 8/9/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class ItemActionListener implements Listener {






    @EventHandler
    public void validateClickedItemAsIcon(InventoryClickEvent event, JavaPlugin plugin)
    {
        if(event != null)
        {
            if(event.getClickedInventory() != null )
            {
                if(AnythingInventory.getInstance().hasMenu(plugin, event.getClickedInventory().getName()))
                {
                    InventoryMenu menu = AnythingInventory.getInstance().getMenu(plugin,event.getClickedInventory().getName());
                    if(menu.containsItem(event.getCurrentItem()))
                    {
                        ItemIconClickedEvent clickedEvent = new ItemIconClickedEvent((Player)event.getWhoClicked(), menu.getItemIcon(event.getCurrentItem()), menu);
                        AnythingInventory.getInstance().getPluginManager().callEvent(clickedEvent);
                    }
                }
            }
        }
    }

    public boolean lockInventoryFromItemStack(InventoryClickEvent event, JavaPlugin plugin)
    {
        if(event != null)
        {
            if(event.getClickedInventory() != null)
            {
                if(AnythingInventory.getInstance().hasMenu(plugin, event.getClickedInventory().getName())) {
                    if (event.getCursor() != null) {
                        event.setCancelled(true);
                        return true;
                    }

                    if (event.getClickedInventory() == event.getWhoClicked().getInventory()) {
                        ItemStack moving = (event.getClick() == ClickType.SHIFT_RIGHT) ? event.getWhoClicked().getInventory().getItem(event.getHotbarButton()) : event.getCursor();

                        if (moving != null) {
                            event.setCancelled(true);
                            return true;
                        }

                    }
                }
            }
        }
        return false;
    }

    public void fireItemEvent(ItemIcon icon, Player player)
    {
        icon.fireItemAction(player);
    }
}

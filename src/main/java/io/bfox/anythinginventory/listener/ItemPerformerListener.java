package io.bfox.anythinginventory.listener;

import io.bfox.anythinginventory.AnythingInventory;
import io.bfox.anythinginventory.ItemPerformer;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by bfox1 on 10/10/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public abstract class ItemPerformerListener
{

    public ItemPerformer validateItemAsPerformer(PlayerInteractEvent event, JavaPlugin plugin)
    {
        if(event != null)
        {
            if(event.getItem() != null)
            {
                if(AnythingInventory.getInstance().isPerformer(plugin, event.getItem().getItemMeta().getDisplayName()))
                {
                        return AnythingInventory.getInstance().getPerformer(plugin, event.getItem().getItemMeta().getDisplayName());
                }
            }
        }
        return null;
    }
}

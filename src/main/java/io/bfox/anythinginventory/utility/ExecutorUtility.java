package io.bfox.anythinginventory.utility;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

/**
 * Created by bfox1 on 9/2/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class ExecutorUtility {

    public static boolean verifyPermission(String permission, Player player) {
        String starOperator = "*";
        return player.hasPermission("AnythingInventory." + starOperator) || player.hasPermission("AnythingInventory.openInventory." + starOperator) || player.hasPermission(permission);
    }

    public static boolean checkCommand(String[] stringArray, int index, String... commandSuffix)
    {
        for (String s : commandSuffix)
        {
            if(stringArray[index].equalsIgnoreCase(s))
            {
                return true;
            }
        }
        return false;
    }

    public static boolean checkCommand(String[] stringArray, int subCommandIndex, int commandInputLength, String... subCommands)
    {
        return checkCommand(stringArray, subCommandIndex, subCommands) && stringArray.length == commandInputLength;
    }


    public static String commandDeniedMessage(String msg)
    {
        return ChatUtility.PLUGIN_NAME + ChatColor.RED + ChatColor.BOLD + msg;
    }
}

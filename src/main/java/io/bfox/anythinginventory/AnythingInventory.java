package io.bfox.anythinginventory;

import io.bfox.anythinginventory.data.InventoryData;
import io.bfox.anythinginventory.init.PluginRegistry;
import io.bfox.anythinginventory.inventory.InventoryMenu;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * Created by bfox1 on 8/1/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 *
 * Powerful, Lightweight, easy to use Menu API.
 */
public class AnythingInventory extends JavaPlugin {

    public FileConfiguration plugin;

    public final Logger logger = Logger.getLogger("Minecraft");

    private final PluginManager pluginManager = this.getServer().getPluginManager();

    @Deprecated
    private final HashMap<String, InventoryMenu> inventoryHashMap = new HashMap<String, InventoryMenu>();
    
    private final Map<JavaPlugin, PluginRegistry> pluginMenuRegistry = new HashMap<>();

    private final Map<Player, ItemPerformer> playerItemPerformerMap = new HashMap<>();

    private final Map<Player, List<ItemStack>> playerDeathMap = new HashMap<>();


    private static AnythingInventory instance;

    public static int SERIAL_VERSION_ID =  1527;



    public void onEnable()
    {
        plugin = this.getConfig();

        if (!this.getDataFolder().exists()) {
            if (this.getDataFolder().mkdir()) ;
        }
        logger.info("Enabling Listeners.");
        instance = this;


    }


    public void onDisable()
    {

       // saveHashData();
    }


    @Deprecated
    private void saveHashData()
    {
        inventoryHashMap.forEach((k, v) -> InventoryData.saveData(this, v));
    }




    public static AnythingInventory getInstance()
    {
        return instance;
    }


    public PluginManager getPluginManager() {
        return pluginManager;
    }
    
    /**
     * Stores the PluginRegistry.
     * Only 1 PluginRegistry can be stored per plugin
     * @param plugin The Plugin in which the Inventory is set to.
     * @param registry The Menu Registry being added.
     */
    public void registerPlugin(JavaPlugin plugin, PluginRegistry registry)
    {
        pluginMenuRegistry.put(plugin, registry);
    }

    public boolean isPluginRegistered(JavaPlugin plugin)
    {
        return pluginMenuRegistry.containsKey(plugin);
    }

    
    public void removeRegistry(JavaPlugin plugin)
    {
    	pluginMenuRegistry.remove(plugin);
    }
    
    /**
     * Returns a List of all inventory Menu from
     * the Plugin its corresponding with.
     * @param plugin
     * @return 
     */
    public List<InventoryMenu> getListByPlugin(JavaPlugin plugin)
    {
    	if(pluginMenuRegistry.containsKey(plugin))
    	{
    		PluginRegistry reg = pluginMenuRegistry.get(plugin);
    		List<InventoryMenu> menuList = new ArrayList<InventoryMenu>();
    		for(Map.Entry<String, InventoryMenu> entry : reg.getMenuHashMap().entrySet())
    		{
    			menuList.add(entry.getValue());
    		}
    		return menuList;
    	}
    	return null;
    }
    
    /**
     * Adds a Menu to the Corresponding PluginRegistry.
     * @param plugin The AnythingInventory Class or Subclass.
     * @param menu
     */
    public void addMenu(JavaPlugin plugin, InventoryMenu menu)
    {
    	pluginMenuRegistry.get(plugin).registerMenu(menu);
    }
    
    /**
     * Removes the Menu from the PluginRegistry.
     * @param plugin
     * @param menuName
     */
    public void removeMenu(JavaPlugin plugin, String menuName)
    {
    	pluginMenuRegistry.get(plugin).unregisterMenu(menuName);
    }

    public boolean hasMenu(JavaPlugin plugin, String menuName)
    {
        if(pluginMenuRegistry.containsKey(plugin))
        {
            PluginRegistry reg = pluginMenuRegistry.get(plugin);
            return reg.hasMenu(menuName);
        }
        return false;
    }
    /**
     * Returns the Menu From the PluginRegistry of the Anything
     * Inventory Class or Subclass.
     * @param plugin
     * @param menuName
     * @return
     */
    public InventoryMenu getMenu(JavaPlugin plugin, String menuName)
    {
    	if(pluginMenuRegistry.containsKey(plugin))
    	{
    		PluginRegistry reg = pluginMenuRegistry.get(plugin);
    		return reg.getMenu(menuName);
    	}
    	return null;
    }

    public CustomAction getAction(JavaPlugin plugin, String actionName)
    {
        if(isPluginRegistered(plugin))
        {
            return pluginMenuRegistry.get(plugin).getAction(actionName);
        }
        return null;
    }

    public ItemPerformer getPerformer(JavaPlugin plugin, String performerName)
    {
        if(isPluginRegistered(plugin))
        {
            return pluginMenuRegistry.get(plugin).getPerformer(performerName);
        }
        return null;
    }


    public void registerAction(JavaPlugin plugin, CustomAction action)
    {
        if(isPluginRegistered(plugin))
        {
            pluginMenuRegistry.get(plugin).registerAction(action);
        }
    }

    public void unregisterAction(JavaPlugin plugin, CustomAction action)
    {
        if(isPluginRegistered(plugin))
        {
            pluginMenuRegistry.get(plugin).unregisterAction(action.getName());
        }
    }

    public boolean hasAction(JavaPlugin plugin, CustomAction action)
    {
        return pluginMenuRegistry.containsKey(plugin) && pluginMenuRegistry.get(plugin).hasAction(action.getName());
    }


    public void registerPerformer(JavaPlugin plugin, ItemPerformer action)
    {
        if(isPluginRegistered(plugin))
        {
            pluginMenuRegistry.get(plugin).registerItemPerformer(action);
        }
    }

    public void unregisterPerformer(JavaPlugin plugin, ItemPerformer action)
    {
        if(isPluginRegistered(plugin))
        {
            pluginMenuRegistry.get(plugin).unregisterPerformer(action.getItemName());
        }
    }

    public boolean isPerformer(JavaPlugin plugin, String action)
    {
        return pluginMenuRegistry.containsKey(plugin) && pluginMenuRegistry.get(plugin).hasPerformer(action);
    }

    public void addPlayerPerformerCooldown(Player player, ItemPerformer performer)
    {
        playerItemPerformerMap.put(player, performer);
    }
    public void removePlayerPerformerCooldown(Player player)
    {
        playerItemPerformerMap.remove(player);
    }

    public boolean isPlayerInCooldown(Player player)
    {
        return playerItemPerformerMap.containsKey(player);
    }

    public void addDeathPlayer(Player player, List<ItemStack> stacks)
    {
        playerDeathMap.put(player, stacks);
    }

    public void removeDeathPlayer(Player player)
    {
        playerDeathMap.remove(player);
    }

    public boolean isPlayerDead(Player player)
    {
        return playerDeathMap.containsKey(player);
    }

    public List<ItemStack> getDeathPlayersStacks(Player player)
    {
        if(isPlayerDead(player))
        {
            return playerDeathMap.get(player);
        }
        return null;
    }
}

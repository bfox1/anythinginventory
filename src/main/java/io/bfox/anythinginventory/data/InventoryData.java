package io.bfox.anythinginventory.data;


import io.bfox.anythinginventory.AnythingInventory;

import io.bfox.anythinginventory.ItemIcon;
import io.bfox.anythinginventory.action.ItemAction;
import io.bfox.anythinginventory.inventory.InventoryMenu;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.io.*;
import java.util.*;

import static io.bfox.anythinginventory.ObjectValidation.*;
/**
 * Purpose: Saves and Loads InventoryMenu Data to a .txt Formatted File in the form
 * of a String.
 *
 * Created by bfox1 on 8/4/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class InventoryData {


    private final AnythingInventory guiMain;

    private File inventoryPath;

    private static File inventoryStaticPath = new File("plugins/AnythingInventory/inventory");

    public InventoryData(AnythingInventory gui)
    {
        checkNotNull(gui);
        inventoryPath = new File("plugins/AnythingInventory/inventory/");
        if(!inventoryPath.exists())
        {
            if(inventoryPath.mkdir());
        }
        this.guiMain = gui;
    }

    /**
     * Save InventoryMenu Data.
     *
     * @param inventory The InventoryMenu to be saved.
     */
    public void saveInventoryData(InventoryMenu inventory)
    {
        checkNotNull(inventory);
        String inventoryName = inventory.getMenuName();

        FileWriter fw = null;
        File file = new File("plugins/AnythingInventory/inventory/" + inventoryName + ".txt");

        try {
            fw = new FileWriter(file);

            fw.write(inventoryToString(inventory));
            fw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    /**
     * Loads the InventoryMenu onto the main class.
     */
    @SuppressWarnings("ConstantConditions")
    public void loadInventoryData()
    {

        for(File file : inventoryPath.listFiles())
        {
            if(file != null)
            {
                InventoryMenu inventory = null;
                try {
                    String inventoryString = new BufferedReader(new FileReader(file)).readLine();
                    inventory = stringToInventory(inventoryString);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if(inventory != null)
               // guiMain.addMenu(inventory);
                //else
                guiMain.logger.severe("Inventory was unsuccessful at reading..");
            }
        }

    }

    /**
     * A static reference to the method loadInventoryData()
     *
     * @param gui The AnythingInventory Class to be referenced
     */
    public static void loadData(AnythingInventory gui)
    {
        new InventoryData(gui).loadInventoryData();
    }

    /**
     * A static reference to the method saveInventoryData()
     *
     * @param gui The AnythingInventory Class to be referenced.
     * @param inventory The InventoryMenu to be Saved.
     */
    public static void saveData(AnythingInventory gui, InventoryMenu inventory)
    {
        new InventoryData(gui).saveInventoryData(inventory);
    }


    /**
     * Get the AnythingInventory Class
     * @return A reference to AnythingInventory
     */
    public AnythingInventory getGuiMain() {
        return guiMain;
    }


    /**
     * Saving the Inventory To String Format
     *
     * @param inventory The InventoryMenu to be saved to a String Format
     * @return A String after InventoryMenu conversion
     */
    private String inventoryToString(InventoryMenu inventory)
    {
        guiMain.logger.info("Preparing To Save Inventory " + inventory.getMenuName());
        String serialization = ":serial@" + AnythingInventory.SERIAL_VERSION_ID + ":size@" +inventory.getInventory().getSize() + ";" + ":name@" +inventory.getMenuName() + ";"
                + ":accept@" +inventory.isAcceptedToAll() + ";" + ":moveFrom@" + inventory.isCanMoveFrom() + ";" + ":moveTo@" + inventory.isCanMoveTo() + ";";



        int i = 0;
        for(ItemStack stack : inventory.getInventory().getContents()) //Looks at the amount of ItemIcons there are.
        {

            if(stack != null) //Checks if object is not null
            {

                ItemIcon iconStack = inventory.getItemIcon(stack);


                String isType = stack.getType().toString();
                String serializedItemStack = "";

                serializedItemStack += "t@" + isType;

                guiMain.logger.info("Building String ItemStack up for saving.");

                if(stack.getDurability() != 0)  //Checks ItemStack of Icon to see if it has a durability
                {
                    String isDurability = String.valueOf(stack.getDurability());
                    serializedItemStack += ":d@" + isDurability;

                    guiMain.logger.info("Writing Durability");
                }
                if(stack.getAmount() != 1)  //Checks ItemStack of Icon to see if it has an amount
                {
                    String isAmount = String.valueOf(stack.getAmount());
                    serializedItemStack += ":a@" + isAmount;

                    guiMain.logger.info("Writing Item Amount");
                }
                Map<Enchantment, Integer> isEnch = stack.getEnchantments();

                if(isEnch.size() > 0) //Checks to see if the Item has any enchantments attached to it.
                {
                    for(Map.Entry<Enchantment, Integer> ench : isEnch.entrySet())
                    {
                        serializedItemStack += ":e@" + ench.getKey().getName() + "@" + ench.getValue();
                    }

                    guiMain.logger.info("Writing Enchants");
                }
                //ItemMetaDataInformation//

                guiMain.logger.info("Checking if Item has ItemMeta");

                if(stack.hasItemMeta()) //Looking into the ItemStack meta to see if it has any meta data.
                {

                    guiMain.logger.info("Building String ItemMeta for " + isType);

                    if(stack.getItemMeta().hasDisplayName()) //Checking to see if it has a Display Name
                    {

                        serializedItemStack += ":n@" + stack.getItemMeta().getDisplayName();
                        guiMain.logger.info("Writing Display Name");

                    }

                    if(stack.getItemMeta().hasLore()) // Checking to see if it has a Lore.
                    {

                        serializedItemStack += ":l@" +stack.getItemMeta().getLore();
                        guiMain.logger.info("Writing Lore");

                    }

                }

                //Dealing with ItemIcon properties//

                guiMain.logger.info("Checking if Item is ItemIcon");
                if(iconStack != null)
                if(iconStack.getAction() != null) //Checking the ItemIcon for an ItemAction
                {
                    guiMain.logger.info("Building String for ItemIcon");
                    serializedItemStack += ":i@" + iconStack.getAction().toString();
                    guiMain.logger.info("Writing ItemAction");
                    guiMain.logger.info("Writing Action details");
                    ActionSerializer id = new ActionSerializer(iconStack.getActionParam());
                    serializedItemStack += ":o@" +  id.ObjectToString();
                }



                serialization += i + "#" + serializedItemStack + ";";
                i++;
            }
        }

        return serialization;
    }

    /**
     * Loading Inventory from String Format
     *
     * @param invString The String of the loaded InventoryMenu
     * @return A InventoryMenu from a loaded String.
     */
    private InventoryMenu stringToInventory(String invString)
    {
        String[] serializedBlocks = invString.split(";");

        String invName = "";
        int invoSize = 0;

        boolean isAcceptedFlag = false;
        boolean canMoveFrom = false;
        boolean canMoveTo = false;

        int versionID = 0;

        System.out.println(serializedBlocks[0]);
        for(String invData : serializedBlocks)
        {
            System.out.println(invData);
            String[] block = invData.split(":");
            String[] invItem = block[0].split("@");
            if(invItem[0].equals("serial"))
            {
                versionID = Integer.valueOf(invItem[1]);
            }
            else if (invItem[0].equals("size"))
            {
                invoSize = Integer.valueOf(invItem[1]);
            }else if(invItem[0].equals("name"))
            {
                invName = invItem[1];
            }
            else if(invItem[0].equals("accept"))
            {
                isAcceptedFlag = Boolean.valueOf(invItem[1]);
            }
            else if(invItem[0].equals("moveFrom"))
            {
                canMoveFrom = Boolean.valueOf(invItem[1]);
            }
            else if(invItem[0].equals("moveTo"))
            {
                canMoveTo = Boolean.valueOf(invItem[1]);
            }
        }

        if(versionID != AnythingInventory.SERIAL_VERSION_ID)
        {
            System.out.println("VersionID does not match!");

            //TODO - Run old loading Format.
        }
        InventoryMenu inventory;
        LinkedHashMap<Integer,ItemIcon> itemIconMap = new LinkedHashMap<Integer,ItemIcon>();

        Inventory deserializedInventory = Bukkit.getServer().createInventory(null,invoSize,invName);
        for (int i = 5; i < serializedBlocks.length; i++)
        {
            String[] serializedBlock = serializedBlocks[i].split("#");
            int stackPosition = Integer.valueOf(serializedBlock[0]);

            if(stackPosition >= deserializedInventory.getSize())
            {
                continue;
            }

            ItemStack stack = null;
            ItemIcon iconStack = null;
            ItemAction action = null;
            Object actionParam = null;
            Boolean createdItemStack = false;

            String[]serializedItemStack = serializedBlock[1].split(":");

            String builtString = "";
            int it = 0;
            for(String itemInfo : serializedItemStack)
            {
                String[] itemAttribute = itemInfo.split("@");
                System.out.println(itemAttribute[0]);
                if(itemAttribute[0].equals("t"))
                {
                    System.out.println("Looking into the ItemAttribute with " + itemAttribute[1]);
                    stack = new ItemStack(Material.getMaterial(itemAttribute[1]));
                    createdItemStack = true;
                }
                else if(itemAttribute[0].equals("d") && createdItemStack)
                {
                    stack.setDurability(Short.valueOf(itemAttribute[1]));
                }
                else if(itemAttribute[0].equals("a") && createdItemStack)
                {
                    stack.setAmount(Integer.valueOf(itemAttribute[1]));
                }
                else if(itemAttribute[0].equals("e") && createdItemStack)
                {
                    stack.addEnchantment(Enchantment.getByName(itemAttribute[1]), Integer.valueOf(itemAttribute[2]));
                }
                else if(itemAttribute[0].equals("n") && createdItemStack)
                {
                    ItemMeta meta = stack.getItemMeta();
                    meta.setDisplayName(itemAttribute[1]);
                    stack.setItemMeta(meta);
                }
                else if(itemAttribute[0].equals("l") && createdItemStack)
                {
                    ItemMeta meta = stack.getItemMeta();
                    String[] loreArray = itemAttribute[1].split("\\[");
                    List<String> lore = new ArrayList<>();
                    for(int f = 0; f < loreArray.length; f++)
                    {

                        if(loreArray[f].contains(","))
                        {
                            loreArray[f] = loreArray[f].substring(0, loreArray[f].length() - 1);
                        }else if(loreArray[f].contains("]"))
                                {
                                    loreArray[f] = loreArray[f].substring(0, loreArray[f].length() - 1);
                                 }
                        if(!loreArray[f].equals(""))
                        {
                            lore.add(loreArray[f]);
                        }
                    }
                    meta.setLore(lore);
                    stack.setItemMeta(meta);
                }
                else if(itemAttribute[0].equals("i") && createdItemStack)
                {
                    action = ItemAction.getItemAction(Integer.valueOf(itemAttribute[1]));

                }
                else if(itemAttribute[0].equals("o") && createdItemStack)
                {
                    guiMain.logger.info("Action Param being Deserialzed.");

                    it++;
                    ActionDeserializer deserializer = new ActionDeserializer(serializedItemStack, action, it);
                    actionParam = deserializer.stringToObject();
                    iconStack = new ItemIcon(stack,action,actionParam);
                    break;
                }
                it++;
            }
            deserializedInventory.setItem(stackPosition, stack);

           // if(iconStack != null)
            //itemIconMap.put(stack.getItemMeta().getDisplayName(), iconStack);

        }

        inventory = new InventoryMenu(deserializedInventory, AnythingInventory.getInstance());
        inventory.setAcceptedToAll(isAcceptedFlag);
        inventory.setCanMoveFrom(canMoveFrom);
        inventory.setCanMoveTo(canMoveTo);
        inventory.setItemIconInventoryList(itemIconMap);
        if(inventory.getInventory().getContents().length == 0)
        {
            return null;
        }
        return inventory;
    }


    /**
     * Gets a List of all available Inventories.
     * @return A list of inventories.
     */
    public static List<String> getInventoryList()
    {
        List<String> stringList = new ArrayList<>();
        for(File file : inventoryStaticPath.listFiles())
        {
            if(file != null)
            {
                stringList.add(file.getName().substring(0, file.getName().length() - 4));
            }
        }
        return stringList;
    }


    /**
     * Purpose: This Inner Class is designed specifically to handle ItemActions that have been serialized
     * by the ActionDeserializer Class.
     */
    private class ActionSerializer
    {
        private Object actionParam;

        public ActionSerializer(Object object)
        {
            this.actionParam = object;
        }

        /**
         * Checks to see if the Object can be identified.
         *
         * @return true if Object have been found, false if not.
         */
        private boolean identifyObject()
        {
            guiMain.logger.info("Identifying ActionParam...");
            if(actionParam instanceof InventoryMenu)
            {
                guiMain.logger.info("Identified Object as InventoryMenu. Preparing for save.");
                return true;
            }else if(actionParam instanceof String)
            {
                guiMain.logger.info("Identified Object as a Command. Preparing for save.");
                return true;
            }else if(actionParam instanceof ItemStack)
            {
                guiMain.logger.info("Identified Object as an ItemStack. Preparing for save.");
                return true;
            }else if(actionParam instanceof Location)
            {
                guiMain.logger.info("Identified Object as a Location. Preparing for save.");
                return true;
            }
            return false;
        }

        /**
         * Convert an ItemStack into a String.
         *
         * @param stack The ItemStack to be Serialized.
         * @return A String from a converted ItemStack.
         */
        private String itemstackToString(ItemStack stack)
        {

            String isType = stack.getType().toString();
            String serializedItemStack = "";

            serializedItemStack += ":t@" + isType;

            if(stack.getDurability() != 0)
            {
                String isDurability = String.valueOf(stack.getDurability());
                serializedItemStack += ":d@" + isDurability;
            }
            if(stack.getAmount() != 1)
            {
                String isAmount = String.valueOf(stack.getAmount());
                serializedItemStack += ":a@" + isAmount;
            }
            Map<Enchantment, Integer> isEnch = stack.getEnchantments();

            if(isEnch.size() > 0)
            {
                for(Map.Entry<Enchantment, Integer> ench : isEnch.entrySet())
                {
                    serializedItemStack += ":e@" + ench.getKey().getName() + "@" + ench.getValue();
                }
            }
            return  serializedItemStack;
        }

        /**
         * Converts a Location into a String.
         *
         * @param location Any location to be Serialized.
         * @return A String from a converted Location.
         */
        private String locationToString(Location location)
        {
            String x = ":x@" +String.valueOf((int)location.getX());
            String y = ":y@" +String.valueOf((int)location.getY());
            String z = ":z@" +String.valueOf((int)location.getZ());
            String yaw = ":w@" +String.valueOf(location.getYaw());
            String pitch = ":p@" + String.valueOf(location.getPitch());
            String world = ":r@" + location.getWorld().getName();

            return x + y + z + yaw + pitch + world;
        }

        /**
         * The Main Action of this Class. To determine what ItemAction the ItemIcon has and
         * how to serialize the ActionParameters.
         *
         * @return A String from the converted ActionParameters.
         */
        public String ObjectToString()
        {
            if(identifyObject())
            {
                if(actionParam instanceof InventoryMenu)
                {
                    return ((InventoryMenu) actionParam).getMenuName();
                }
                if(actionParam instanceof String)
                {
                    return (String) actionParam;
                }
                if(actionParam instanceof ItemStack)
                {
                    return itemstackToString((ItemStack) actionParam);
                }
                if(actionParam instanceof Location)
                {
                    return locationToString((Location) actionParam);
                }
            }
            return null;
        }

    }

    /**
     * Converting String into Object
     */
    private class ActionDeserializer
    {
        private Object actionParam;
        private String[] objectString;
        private ItemAction action;
        private int index =0;

        public ActionDeserializer(String[] objectString, ItemAction action, int id)
        {
            this.objectString = objectString;
            this.action = action;
            this.index = id;
        }

        public Object stringToObject()
        {
            if(action == ItemAction.ITEM)
            {
                guiMain.logger.info("Action is an Item, Beginning to deserialize String\n");
                return stringToItemStack(objectString);
            }
            else if(action == ItemAction.TELEPORT)
            {
                guiMain.logger.info("Action is a Teleport, Beginning to deserialized String\n");
                return stringToLocation(objectString);
            }
            else if(action == ItemAction.INVENTORY)
            {
                guiMain.logger.info("Action is an Inventory, Beginning to deserialized String\n");
                if(loadInventoryFromString(objectString))
                {
                    return objectString;
                }
            }
            else if(action == ItemAction.COMMAND)
            {
                guiMain.logger.info("Action is a Command, Beginning to deserialized String\n");
                return objectString;
            }
            return null;
        }

        private ItemStack stringToItemStack(String[] item)
        {
            ItemStack stack = null;
            Boolean createdItemStack = false;
            for (int i = index; i < item.length; i++)
            {
                String[] itemAttribute = item[i].split("@");
                if (itemAttribute[0].equals("t"))
                {
                    stack = new ItemStack(Material.getMaterial(itemAttribute[1]));
                    createdItemStack = true;
                }
                else if (itemAttribute[0].equals("d") && createdItemStack)
                {
                    stack.setDurability(Short.valueOf(itemAttribute[1]));
                }
                else if (itemAttribute[0].equals("a") && createdItemStack)
                {
                    stack.setAmount(Integer.valueOf(itemAttribute[1]));
                }
                else if (itemAttribute[0].equals("e") && createdItemStack)
                {
                    stack.addEnchantment(Enchantment.getById(Integer.valueOf(itemAttribute[1])), Integer.valueOf(itemAttribute[2]));
                }
                else if(itemAttribute[0].equals("n") && createdItemStack)
                {
                    ItemMeta meta = stack.getItemMeta();
                    meta.setDisplayName(itemAttribute[1]);
                    stack.setItemMeta(meta);
                }
                else if(itemAttribute[0].equals("l") && createdItemStack)
                {
                    ItemMeta meta = stack.getItemMeta();
                    String[] loreArray = itemAttribute[1].split("]");
                    meta.setLore(Arrays.asList(loreArray));
                    stack.setItemMeta(meta);
                }
            }
            return stack;
        }

        private Location stringToLocation(String[] locationString)
        {
            Location loc;
            int x =0, y=0, z = 0;
            float yaw=0.0F, pitch = 0.0F;
            World world = null;

            System.out.println("Preparing for unloading");
            for(int i = index; i < locationString.length; i++)
            {
                System.out.println(locationString[i]);
                String[] locAttribute = locationString[i].split("@");
                System.out.println(locAttribute[1]);
                if (locAttribute[0].equals("x")) {
                    x = Integer.valueOf(locAttribute[1]);
                } else if (locAttribute[0].equals("y")) {
                    y = Integer.valueOf(locAttribute[1]);
                } else if (locAttribute[0].equals("z")) {
                    z = Integer.valueOf(locAttribute[1]);
                } else if (locAttribute[0].equals("w")) {
                    yaw = Float.valueOf(locAttribute[1]);
                } else if (locAttribute[0].equals("p")) {
                    pitch = Float.valueOf(locAttribute[1]);
                } else if (locAttribute[0].equals("r")) {
                    System.out.println(locAttribute[1]);
                    world = Bukkit.getWorld(locAttribute[1]);
                }
            }
            loc = new Location(world, x, y, z, yaw, pitch);
            return loc;
        }

        private boolean loadInventoryFromString(String[] inventoryName)
        {
            String[] invo = inventoryName[0].split("@");
            if(invo[0].equals("v")) {
                File file = new File("plugins/InventoryMenu/inventory/" + invo[1] + ".txt");
                return file.exists();
            }
            return false;
        }

    }

}



package io.bfox.anythinginventory;

/**
 * Created by bfox1 on 8/4/2015.
 */
public class ObjectValidation
{


    public static void checkNotNull(Object object)
    {
        if(object == null)
        {
            throw new NullPointerException("Object cannot be null");
        }
    }
}

package io.bfox.anythinginventory.init;

import java.util.HashMap;
import java.util.Map;

import io.bfox.anythinginventory.AnythingInventory;
import io.bfox.anythinginventory.CustomAction;
import io.bfox.anythinginventory.ItemPerformer;
import io.bfox.anythinginventory.inventory.InventoryMenu;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by bfox1 on 9/17/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 * Each Plugin that will uses AnythingInventoryAPI is required to register there Plugin here.
 * Otherwise, AnythingInventory couldn't Load your menus!
 */
public class PluginRegistry {
	
	private final JavaPlugin plugin;

	private final Map<String, InventoryMenu> menuMap = new HashMap<String, InventoryMenu>();

	private final Map<String, CustomAction> actionMap = new HashMap<>();

    private final Map<String, ItemPerformer> performerMap = new HashMap<>();
	
	private PluginRegistry(JavaPlugin plugin)
	{
		this.plugin = plugin;
	}
	
	
	public JavaPlugin getPlugin()
	{
		return plugin;
	}

	/**
	 * Registers the Menu.
	 * @param menu The Menu
	 */
	public void registerMenu(InventoryMenu menu)
	{
		menuMap.put(menu.getMenuName(), menu);
	}

    public void registerAction(CustomAction action)
    {
        actionMap.put(action.getName(), action);
    }
    public void registerItemPerformer(ItemPerformer performer)
    {
        if(hasPerformer(performer.getItemName()))
        {
            throw new IllegalArgumentException("Cannot register ItemPerformer with the Same name!");
        }
        else
        {
            performerMap.put(performer.getItemName(), performer);
        }
    }

    public boolean hasPerformer(String perfoName)
    {
        return performerMap.containsKey(perfoName);
    }

	
	public boolean hasMenu(String menuName)
	{
		return menuMap.containsKey(menuName);
	}

    public boolean hasAction(String actionName)
    {
        return actionMap.containsKey(actionName);
    }

    public void unregisterAction(String name)
    {
        if(hasAction(name))
        {
            actionMap.remove(name);
        }
    }
	
	public void unregisterMenu(String menu)
	{
		if(hasMenu(menu))
		{
			menuMap.remove(menu);
		}
	}

    public void unregisterPerformer(String performer)
    {
        if(hasPerformer(performer))
        {
            performerMap.remove(performer);
        }
    }
	
	/**
	 * Returns InventoryMenu.
	 * @param menuName The Menu Name.
	 * @return The InventoryMenu.
	 */
	public InventoryMenu getMenu(String menuName)
	{
		return hasMenu(menuName) ? menuMap.get(menuName) : null;
	}

    public CustomAction getAction(String actionName)
    {
        return hasAction(actionName) ? actionMap.get(actionName) : null;
    }

    public ItemPerformer getPerformer(String performer)
    {
        return hasPerformer(performer) ? performerMap.get(performer) : null;
    }
	
	public Map<String, InventoryMenu> getMenuHashMap()
	{
		return menuMap;
	}
	
	
	public static void initMenuRegistry(JavaPlugin plugin)
	{
		AnythingInventory.getInstance().registerPlugin(plugin, new PluginRegistry(plugin));
	}




}

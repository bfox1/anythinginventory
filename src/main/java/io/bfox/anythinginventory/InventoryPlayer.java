package io.bfox.anythinginventory;

import org.bukkit.entity.Player;

import java.util.UUID;

/**
 * Created by bfox1 on 8/29/2015.
 * Deuteronomy 8:18
 * 1 Peter 4:10
 */
public class InventoryPlayer {

    private final AnythingInventory plugin;
    private final Player player;
    private final UUID player_UUID;
    private String playerName;
    private boolean inEditMode;
    private String editingInvoName;
    private ItemIcon buildingIcon;

    public InventoryPlayer(AnythingInventory plugin, Player player)
    {
        this.player = player;
        this.playerName = player.getDisplayName();
        this.plugin = plugin;
        player_UUID = player.getUniqueId();
    }

    public AnythingInventory getPlugin() {
        return plugin;
    }

    public Player getPlayer() {
        return player;
    }

    public UUID getPlayerUUID() {
        return player_UUID;
    }

    public String getPlayerName() {
        return playerName;
    }

    public boolean isInEditMode() {
        return inEditMode;
    }

    public void setInEditMode(boolean inEditMode) {
        this.inEditMode = inEditMode;
    }

    public String getEditingInvoName() {
        return editingInvoName;
    }

    public void setEditingInvoName(String editingInvoName) {
        this.editingInvoName = editingInvoName;
    }

    public ItemIcon getBuildingIcon() {
        return buildingIcon;
    }

    public void setBuildingIcon(ItemIcon buildingIcon) {
        this.buildingIcon = buildingIcon;
    }
}
